package org.blog.test.configuration.datasource.model;

public enum DbType {
    MASTER,
    SLAVE
}