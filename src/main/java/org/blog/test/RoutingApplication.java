package org.blog.test;

import lombok.extern.slf4j.Slf4j;
import org.blog.test.person.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@Slf4j
public class RoutingApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(RoutingApplication.class, args);
    }

    @Autowired
    private PersonService personService;

    @Override
    public void run(String... args) throws Exception {
        log.info("personService.getPerson : {}", personService.getPerson(1));
        log.info("personService.getPersonFromSlave : {}", personService.getPersonFromSlave(1));
    }
}